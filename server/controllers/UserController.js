const UserModel = require('./../models/UserModel');
const UserController = {
    create(req, res) {
        if (!req.body.name && !req.body.email) {
            return res.status(400).send({
                'message': 'All fields are required'
            })
        }
        const newUser = UserModel.create(req.body);
        return res.status(201).send(newUser);
    }
}

module.exports = UserController;