class UserRoles {
    static getAdminRole() {
        return 'admin';
    }
    static getUserRole() {
        return 'user';
    }
}

module.exports = UserRoles