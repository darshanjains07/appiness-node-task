const {
    uuid
} = require('uuidv4');

const UserRoles = require('./UserRoles');

class UserModel {

    constructor() {
        this.users = [];
    }

    create(data) {
        const newUser = {
            id: uuid(),
            name: data.username || '',
            email: data.email || '',
            role: (this.users.length == 0) ? UserRoles.getAdminRole() : UserRoles.getUserRole()
        };
        this.users.push(newUser);
        console.log(this.users);
        return newUser;
    }


}

module.exports = new UserModel