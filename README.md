## Appiness Node Task Installation

Clone repo and Run `npm install`

## Development server

Goto directory and Run `node server.js` for a dev server. it will run on http://localhost:3000

## Run Api.

Make a post request to http://localhost:3000/api/v1/users from postman
pass JSON object to create new user. 

{
	"username":"Darshan Jain",
	"email":"dj@th2.in"
} 

you will get the response like this
{
    "id": "4c4736fc-649a-4c9f-8280-dac304e4ebeb",
    "name": "Darshan Jain",
    "email": "dj@th2.in",
    "role": "admin" // only for first user.
}

