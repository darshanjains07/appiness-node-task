// Get dependencies
const app = require("./server/app");
const http = require('http');

const server = http.createServer(app);

const port = process.env.PORT || '3000';
app.set('port', port);

/**
 * Listen on provided port, on all network interfaces.
 */
server.listen(port, () => console.log(`API running on localhost:${port}`));